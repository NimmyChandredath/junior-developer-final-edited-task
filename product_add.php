<div class="add_product">
    <nav class="navbar navbar-expand-md  fixed-top">
        <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
            <div class="navbar-btn btn-md mr-auto">
                <p><u><h4>Add Produc</h4></u></p>
            </div>
        </div>
        <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
            <div class="navbar-nav ml-auto">
                <button type="submit" name= "save" class="btn btn-secondary ml-5" >Save</button>
            </div>
        </div>
    </nav>
</div>
<?php
    require ("header.php");
    require("product_database.php");
    Add_Product();
    function Add_Product(){
?>
<div class="container col-md-3 form-group" style="margin-top: 60px; margin-bottom: 1px;">
    <form method="post">
        <div class="" align="left">
            <label for="sku">SKU</label>
            <input type="text" class="form-control" name="sku" id="sku" required>
        </div>
        <div  align="left">
            <label for="prdname">Name</label>
            <input type="text" name="prdname" id="prdname" class="form-control" required>
        </div>
        <div align="left">
            <label for="prdprice">Price:</label>
            <input type="text" name="prdprice" class="form-control" id="prdprice" required>
        </div> 
        <br>
        <div align="left">
            <select name="type" id="target" class="dropdown">
                <option value="">Type Switcher...</option>
                <option value="dvd">DVD-disc</option>
                <option value="book">Book</option>
                <option value="furniture">Furniture</option>
            </select>
        </div>
        <br>
        <div id="dvd" class="inv" >
            <label for="mb">Size</label>
            <input type="text" name="dvd" class="form-control" id="mb">
        </div>
        <div id="book" class="inv" >
            <label for="kg">Weight</label>
            <input type="text" name="kg" class="form-control" id="kg">
        </div>
        <div  id="furniture" class="inv" >
            <label for="ht">Height</label>
            <input type="text" name="ht" class="form-control" id="ht"><br>
            <label for="wt">Width</label>
            <input type="text" name="wt" class="form-control" id="wt"><br>
            <label for="lt">Length</label>
            <input type="text" name="lt" class="form-control" id="lt"><br>
        </div>     
    </form> 
</div>
<?php 
}
?>
<script>
    document
        .getElementById('target')
        .addEventListener('change', function () {
            'use strict';
            var vis = document.querySelector('.vis'),
            target = document.getElementById(this.value);
            if (vis !== null) {
                vis.className = 'inv';
            }
            if (target !== null) {
                target.className = 'vis';
            }
        });
</script>
   

