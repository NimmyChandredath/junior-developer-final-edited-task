-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 23, 2019 at 05:08 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prod`
--

-- --------------------------------------------------------

--
-- Table structure for table `prod_book`
--

DROP TABLE IF EXISTS `prod_book`;
CREATE TABLE IF NOT EXISTS `prod_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sku` varchar(20) NOT NULL,
  `name` text NOT NULL,
  `price` int(20) NOT NULL,
  `book_weight` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prod_book`
--

INSERT INTO `prod_book` (`id`, `sku`, `name`, `price`, `book_weight`) VALUES
(17, '2019Bo3', 'WINGS', 5, 1),
(15, '2019Bo5', 'WINGS\r\n', 15, 2),
(16, '2019bo2', 'WINGS', 40, 3),
(18, '2019bo4', 'WINGS', 5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `prod_dvd`
--

DROP TABLE IF EXISTS `prod_dvd`;
CREATE TABLE IF NOT EXISTS `prod_dvd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sku` varchar(20) NOT NULL,
  `name` text NOT NULL,
  `price` int(20) NOT NULL,
  `dvd_size` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prod_dvd`
--

INSERT INTO `prod_dvd` (`id`, `sku`, `name`, `price`, `dvd_size`) VALUES
(36, '2019DV4', 'ACME', 5, 200),
(33, '2019DV1', 'ACME', 5, 200),
(34, '2019DV2', 'ACME ', 20, 5),
(35, '2019DV3', 'ACME ', 15, 150);

-- --------------------------------------------------------

--
-- Table structure for table `prod_furt`
--

DROP TABLE IF EXISTS `prod_furt`;
CREATE TABLE IF NOT EXISTS `prod_furt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sku` varchar(20) NOT NULL,
  `name` text NOT NULL,
  `price` int(11) NOT NULL,
  `furt_height` int(11) NOT NULL,
  `furwidth` int(11) NOT NULL,
  `furlength` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prod_furt`
--

INSERT INTO `prod_furt` (`id`, `sku`, `name`, `price`, `furt_height`, `furwidth`, `furlength`) VALUES
(33, '2019FU3', 'SHELF', 100, 20, 30, 30),
(31, '2019FU1', 'CHAIR', 300, 10, 11, 12),
(32, '2019FU2', 'DESK', 50, 10, 20, 30),
(34, '2019FU4', 'WINDOW', 400, 60, 20, 40);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
