<?php
	require_once('header.php');
	require_once('product_database.php');
	$res_dvd = $database->readdvd();//calling the readdvd() to display the records of dvd table
	$res_book = $database->readbook();//calling the readbook() to display the records of book table
	$res_furt = $database->readfurt();//calling the readfurt() to display the records of furt table
?>
<h5>Product List</h5>
<?php
	while($row = mysqli_fetch_assoc($res_dvd)){
?>
<form action="delete_product.php" method="post">
	<div class="container" style="display: block;">
		<div class="card" style="float: left; margin-left: 5px; margin-right: 25px; padding: 20px 20px; width: 230px; margin-top: 40px; margin-bottom: 1px; text-align: center;" >
			<input type="checkbox" name="checkbox[]" value= <?php echo $row['id']; ?>>
			<?php echo $row['sku']. "<br>"; ?>
			<?php echo $row['name']. "<br>"; ?>
			<?php echo $row['price']. "$" .  "<br>"; ?>
			<?php echo "Size:". $row['dvd_size']. "MB"; ?>
		</div>
	</div>
<?php 
}
?>
<?php
	while($row = mysqli_fetch_assoc($res_book)){
?>
	<div class="container" style="display: block;">
		<div class="card" style="float: left; margin-left: 5px; margin-right: 25px; padding: 20px 20px; width: 230px; margin-top: 60px; margin-bottom: 1px; text-align: center;" >
			<input type="checkbox" name="checkbox[]" value= <?php echo $row['id']; ?>> 
			<?php echo $row['sku']. "<br>"; ?> 
			<?php echo $row['name']. "<br>"; ?>
			<?php echo $row['price']. "$" . "<br>"; ?>
			<?php echo "Weight:". $row['book_weight']. "KG". "<br>"; ?>
		</div>
	</div>
<?php 
} 
?>
<?php
	while($row = mysqli_fetch_assoc($res_furt)){
?>
	<div class="container" style="display: block;">
		<div class="card" style="float: left; margin-left: 5px; margin-right: 25px; padding: 20px 20px; width: 230px; margin-top: 60px; margin-bottom: 1px; text-align: center;" >
			<input type="checkbox" name="checkbox[]" value= <?php echo $row['id']; ?>>
			<?php echo $row['sku']. "<br>"; ?>
			<?php echo $row['name']. "<br>"; ?>
			<?php echo $row['price']. "$" . "<br>"; ?>
			<?php echo "Dimension:". $row["furt_height"] . "x" . $row["furwidth"]. "x" .  $row["furlength"]. " " . "<br>" ; ?>
		</div>
	</div>
<?php
} 
?>
	<select id="product" name="product">
	    <option selected>Delete Action</option>
	    <option>DVD</option>
	    <option>BOOK</option>
	    <option>FURNINITURE</option>
  	</select> 
	<input type="submit" name="delete" value="Apply" />
</form>
